<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Produit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProduitRepository")
 */

class Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="designation", type="string" , length=255 , nullable=true)
     */
    private $designation;

    /**
     * @var string
     * @ORM\Column(name="fabricant", type="string" , length=255 , nullable=true)
     */
    private $fabricant;

    /**
     * @var string
     * @ORM\Column(name="lien", type="string" , length=255 , nullable=true)
     */
    private $lien;

    /**
     * @var string
     * @ORM\Column(name="norme", type="string" , length=255 , nullable=true)
     */
    private $norme;

    /**
     * @var string
     * @ORM\Column(name="reforigine", type="string" , length=255 , nullable=true)
     */
    private $reforigine;

    /**
     * @var string
     * @ORM\Column(name="reffabricant", type="string" , length=255 , nullable=true)
     */
    private $reffabricant;

    /**
     * @var string
     * @ORM\Column(name="code", type="string" , length=255 , nullable=true)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;


    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", nullable=true)
     */
    private $ref;

    /**
     * @var float
     *
     * @ORM\Column(name="ht", type="float", nullable=true)
     */
    private $ht;

    /**
     * @var float
     *
     * @ORM\Column(name="ttc", type="float", nullable=true)
     */
    private $ttc;

    /**
     * @var boolean
     *
     * @ORM\Column(name="topNov", type="boolean", nullable=true)
     */
    private $topNov;

    /**
     * @var text
     *
     * @ORM\Column(name="descriptif", type="text", nullable=true)
     */
    private $descriptif;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Puissance", inversedBy="Produit"))
     * @ORM\JoinColumn(name="Puissanceid", referencedColumnName="id" , nullable=true)
     */
    private $Puissanceid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Marque", inversedBy="Produit"))
     * @ORM\JoinColumn(name="Marqueid", referencedColumnName="id" , nullable=true)
     */
    private $Marqueid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Modele", inversedBy="Produit"))
     * @ORM\JoinColumn(name="Modeleid", referencedColumnName="id" , nullable=true)
     */
    private $Modeleid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cylindree", inversedBy="Produit"))
     * @ORM\JoinColumn(name="Cylindreeid", referencedColumnName="id" , nullable=true)
     */
    private $Cylindreeid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Type_moteur", inversedBy="Produit"))
     * @ORM\JoinColumn(name="Type_moteurid", referencedColumnName="id" , nullable=true)
     */
    private $Type_moteurid;

    private  $file;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", nullable=true)
     */
    private $logo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getFabricant()
    {
        return $this->fabricant;
    }

    /**
     * @param string $fabricant
     */
    public function setFabricant($fabricant)
    {
        $this->fabricant = $fabricant;
    }

    /**
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * @param string $lien
     */
    public function setLien($lien)
    {
        $this->lien = $lien;
    }

    /**
     * @return string
     */
    public function getReffabricant()
    {
        return $this->reffabricant;
    }

    /**
     * @param string $reffabricant
     */
    public function setReffabricant($reffabricant)
    {
        $this->reffabricant = $reffabricant;
    }

    /**
     * @return string
     */
    public function getReforigine()
    {
        return $this->reforigine;
    }

    /**
     * @param string $reforigine
     */
    public function setReforigine($reforigine)
    {
        $this->reforigine = $reforigine;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $reforigine
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getNorme()
    {
        return $this->norme;
    }

    /**
     * @param string $norme
     */
    public function setNorme($norme)
    {
        $this->norme = $norme;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }
    
    

    /**
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param string $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return boolean
     */
    public function getTopNov()
    {
        return $this->topNov;
    }

    /**
     * @param boolean $topNov
     */
    public function setTopNov($topNov)
    {
        $this->topNov = $topNov;
    }

    /**
     * @return float
     */
    public function getHt()
    {
        return $this->ht;
    }

    /**
     * @param float $ht
     */
    public function setHt($ht)
    {
        $this->ht = $ht;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }
    

    /**
     * @return float
     */
    public function getTtc()
    {
        return $this->ttc;
    }

    /**
     * @param float $ttc
     */
    public function setTtc($ttc)
    {
        $this->ttc = $ttc;
    }


    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }


    /**
     * @return mixed
     */
    public function getMarqueid()
    {
        return $this->Marqueid;
    }

    /**
     * @param mixed $Marqueid
     */
    public function setMarqueid($Marqueid)
    {
        $this->Marqueid = $Marqueid;
    }

    /**
     * @return mixed
     */
    public function getModeleid()
    {
        return $this->Modeleid;
    }

    /**
     * @param mixed $Modeleid
     */
    public function setModeleid($Modeleid)
    {
        $this->Modeleid = $Modeleid;
    }

    /**
     * @return mixed
     */
    public function getCylindreeid()
    {
        return $this->Cylindreeid;
    }

    /**
     * @param mixed $Cylindreeid
     */
    public function setCylindreeid($Cylindreeid)
    {
        $this->Cylindreeid = $Cylindreeid;
    }

    /**
     * @return mixed
     */
    public function getType_moteurid()
    {
        return $this->Type_moteurid;
    }

    /**
     * @param mixed $Type_moteurid
     */
    public function setType_moteurid($Type_moteurid)
    {
        $this->Type_moteurid = $Type_moteurid;
    }

    /**
     * @return mixed
     */
    public function getTypeMoteurid()
    {
        return $this->Type_moteurid;
    }

    /**
     * @param mixed $Type_moteurid
     */
    public function setTypeMoteurid($Type_moteurid)
    {
        $this->Type_moteurid = $Type_moteurid;
    }


    /**
     * @return mixed
     */
    public function getPuissanceid()
    {
        return $this->Puissanceid;
    }

    /**
     * @param mixed $Puissanceid
     */
    public function setPuissanceid($Puissanceid)
    {
        $this->Puissanceid = $Puissanceid;
    }





  



}
