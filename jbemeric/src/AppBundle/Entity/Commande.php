<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Annotations\Annotation ;
/**
 * Commande
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CommandeRepository")
 */

class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="ref", type="string" , length=255 , nullable=true)
     */
    private $ref;
    
    /**
     * @Assert\Date()
 *
 * @ORM\Column(name="dateLivraison", type="date", nullable=true)
 */
    private $dateLivraison;

    /**
     * @Assert\Date()
     *
     * @ORM\Column(name="dateCommande", type="date", nullable=true)
     */
    private $dateCommande;

    /**
     * @var string
     *
     * @ORM\Column(name="adresseLivraison", type="string", nullable=true)
     */
    private $adresseLivraison;

    /**
     * @var string
     *
     * @ORM\Column(name="adresseCommande", type="string", nullable=true)
     */
    private $adresseCommande;

    /**
     * Bidirectional - Many users have Many favorite comments (OWNING SIDE)
     *
     * @ORM\ManyToMany(targetEntity="Produit", inversedBy="Commande")
     * @ORM\JoinTable(name="ligneCommande")
     */
    private $ligneCommande;


    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="Commande"))
     * @ORM\JoinColumn(name="Userid", referencedColumnName="id" , nullable=true)
     */
    private $Userid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return string
     */
    public function getDateLivraison()
    {
        return $this->dateLivraison;
    }

    /**
     * @param string $dateLivraison
     */
    public function setDateLivraison($dateLivraison)
    {
        $this->dateLivraison = $dateLivraison;
    }

    /**
     * @return string
     */
    public function getDateCommande()
    {
        return $this->dateCommande;
    }

    /**
     * @param string $dateCommande
     */
    public function setDateCommande($dateCommande)
    {
        $this->dateCommande = $dateCommande;
    }

    /**
     * @return string
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @param string $adresseLivraison
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;
    }

    /**
     * @return string
     */
    public function getAdresseCommande()
    {
        return $this->adresseCommande;
    }

    /**
     * @param string $adresseCommande
     */
    public function setAdresseCommande($adresseCommande)
    {
        $this->adresseCommande = $adresseCommande;
    }

    /**
     * @return mixed
     */
    public function getLigneCommande()
    {
        return $this->ligneCommande;
    }

    /**
     * @param mixed $ligneCommande
     */
    public function setLigneCommande($ligneCommande)
    {
        $this->ligneCommande = $ligneCommande;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    public function addLigneCommande ($ligneCommande)
    {
        $this->getLigneCommande()->add($ligneCommande);
    }

    public function removeLigneCommande ($ligneCommande)
    {
        $this->getLigneCommande()->removeElement($ligneCommande);
    }

    /**
     * @return mixed
     */
    public function getUserid()
    {
        return $this->Userid;
    }

    /**
     * @param mixed $Userid
     */
    public function setUserid($Userid)
    {
        $this->Userid = $Userid;
    }



}
