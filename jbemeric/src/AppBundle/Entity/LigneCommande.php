<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Annotations\Annotation ;
/**
 * LigneCommande
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\LigneCommandeRepository")
 */

class LigneCommande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="quantite", type="integer" , length=255 , nullable=true)
     */
    private $quantite;

    /**
     * @var float
     * @ORM\Column(name="prix", type="float" , length=255 , nullable=true)
     */
    private $prix;

    /**
     * @var float
     * @ORM\Column(name="total", type="float" , length=255 , nullable=true)
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Produit", inversedBy="LigneCommande"))
     * @ORM\JoinColumn(name="Produitid", referencedColumnName="id" , nullable=true)
     */
    private $Produitid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Commande", inversedBy="LigneCommande"))
     * @ORM\JoinColumn(name="Commandeid", referencedColumnName="id" , nullable=true)
     */
    private $Commandeid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

    /**
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getProduitid()
    {
        return $this->Produitid;
    }

    /**
     * @param mixed $Produitid
     */
    public function setProduitid($Produitid)
    {
        $this->Produitid = $Produitid;
    }

    /**
     * @return mixed
     */
    public function getCommandeid()
    {
        return $this->Commandeid;
    }

    /**
     * @param mixed $Commandeid
     */
    public function setCommandeid($Commandeid)
    {
        $this->Commandeid = $Commandeid;
    }



}
