<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AutreMetas
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AutreMetasRepository")
 */

class AutreMetas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="author", type="string" , length=255 , nullable=true)
     */
    private $author;

    /**
     * @var string
     * @ORM\Column(name="subject", type="string" , length=255 , nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="revisitAfter", type="string", nullable=true)
     */
    private $revisitAfter;

    /**
     * @var string
     *
     * @ORM\Column(name="canonical", type="string", nullable=true)
     */
    private $canonical;

    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="string", nullable=true)
     */
    private $rating;


    /**
     * @var string
     *
     * @ORM\Column(name="organization", type="string", nullable=true)
     */
    private $organization;

    /**
     * @var string
     *
     * @ORM\Column(name="classification", type="string", nullable=true)
     */
    private $classification;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="distribution", type="string", nullable=true)
     */
    private $distribution;

    /**
     * @var string
     *
     * @ORM\Column(name="audience", type="string", nullable=true)
     */
    private $audience;

    /**
     * @var string
     *
     * @ORM\Column(name="publisher", type="string", nullable=true)
     */
    private $publisher;

    /**
     * @var string
     *
     * @ORM\Column(name="ogType", type="string", nullable=true)
     */
    private $ogType;

    /**
     * @var string
     *
     * @ORM\Column(name="ogSiteName", type="string", nullable=true)
     */
    private $ogSiteName;

    /**
     * @var string
     *
     * @ORM\Column(name="ogImage", type="string", nullable=true)
     */
    private $ogImage;

    /**
     * @var string
     *
     * @ORM\Column(name="ogImageType", type="string", nullable=true)
     */
    private $ogImageType;

    /**
     * @var string
     *
     * @ORM\Column(name="copyright", type="string", nullable=true)
     */
    private $copyright;

    /**
     * @var string
     *
     * @ORM\Column(name="twitterCard", type="string", nullable=true)
     */
    private $twitterCard;

    /**
     * @var string
     *
     * @ORM\Column(name="twitterCreator", type="string", nullable=true)
     */
    private $twitterCreator;

    /**
     * @var string
     *
     * @ORM\Column(name="twittertitle", type="string", nullable=true)
     */
    private $twittertitle;

    /**
     * @var string
     *
     * @ORM\Column(name="twitterDescription", type="string", nullable=true)
     */
    private $twitterDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="twitterImage", type="string", nullable=true)
     */
    private $twitterImage;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }




    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }
    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getRevisitAfter()
    {
        return $this->revisitAfter;
    }

    /**
     * @param string $revisitAfter
     */
    public function setRevisitAfter($revisitAfter)
    {
        $this->revisitAfter = $revisitAfter;
    }
    /**
     * @return string
     */
    public function getCanonical()
    {
        return $this->canonical;
    }

    /**
     * @param string $canonical
     */
    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }


    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
    /**
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param string $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return string
     */
    public function getClassification()
    {
        return $this->classification;
    }

    /**
     * @param string $classification
     */
    public function setClassification($classification)
    {
        $this->classification = $classification;
    }
    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $canonical
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }


    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }
    /**
     * @return string
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * @param string $distribution
     */
    public function setDistribution($distribution)
    {
        $this->distribution = $distribution;
    }

    /**
     * @return string
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * @param string $audience
     */
    public function setAudience($audience)
    {
        $this->audience = $audience;
    }
    /**
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param string publisher
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
    }


    /**
     * @return string
     */
    public function getOgType()
    {
        return $this->ogType;
    }

    /**
     * @param string $ogType
     */
    public function setOgType($ogType)
    {
        $this->ogType = $ogType;
    }
    /**
     * @return string
     */
    public function getOgSiteName()
    {
        return $this->ogSiteName;
    }

    /**
     * @param string $ogSiteName
     */
    public function setOgSiteName($ogSiteName)
    {
        $this->ogSiteName = $ogSiteName;
    }

    /**
     * @return string
     */
    public function getOgImage()
    {
        return $this->ogImage;
    }

    /**
     * @param string $ogImage
     */
    public function setOgImage($ogImage)
    {
        $this->ogImage = $ogImage;
    }
    /**
     * @return string
     */
    public function getOgImageType()
    {
        return $this->ogImageType;
    }

    /**
     * @param string $ogImageType
     */
    public function setOgImageType($ogImageType)
    {
        $this->ogImageType = $ogImageType;
    }

    /**
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param string $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }
    /**
     * @return string
     */
    public function getTwitterCard()
    {
        return $this->twitterCard;
    }

    /**
     * @param string $twitterCard
     */
    public function setTwitterCard($twitterCard)
    {
        $this->twitterCard = $twitterCard;
    }




    /**
     * @return string
     */
    public function getTwitterCreator()
    {
        return $this->twitterCreator;
    }

    /**
     * @param string $twitterCreator
     */
    public function setTwitterCreator($twitterCreator)
    {
        $this->twitterCreator = $twitterCreator;
    }
    /**
     * @return string
     */
    public function getTwittertitle()
    {
        return $this->twittertitle;
    }

    /**
     * @param string $twittertitle
     */
    public function setTwittertitle($twittertitle)
    {
        $this->twittertitle = $twittertitle;
    }

    /**
     * @return string
     */
    public function getTwitterDescription()
    {
        return $this->twitterDescription;
    }

    /**
     * @param string $twitterDescription
     */
    public function setTwitterDescription($twitterDescription)
    {
        $this->twitterDescription = $twitterDescription;
    }
    /**
     * @return string
     */
    public function getTwitterImage()
    {
        return $this->twitterImage;
    }

    /**
     * @param string $twitterImage
     */
    public function setTwitterImage($twitterImage)
    {
        $this->twitterImage = $twitterImage;
    }


}
