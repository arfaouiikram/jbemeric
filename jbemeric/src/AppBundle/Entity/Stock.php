<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Stock
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\StockRepository")
 */

class Stock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="quantite", type="integer" , length=255 , nullable=true)
     */
    private $quantite;
    


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Produit", inversedBy="Accessoire"))
     * @ORM\JoinColumn(name="Produitid", referencedColumnName="id" , nullable=true)
     */
    private $Produitid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Depot", inversedBy="Stock"))
     * @ORM\JoinColumn(name="Depotid", referencedColumnName="id" , nullable=true)
     */
    private $Depotid;
    

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

    

    /**
     * @return mixed
     */
    public function getProduitid()
    {
        return $this->Produitid;
    }

    /**
     * @param mixed $Produitid
     */
    public function setProduitid($Produitid)
    {
        $this->Produitid = $Produitid;
    }

    /**
     * @return mixed
     */
    public function getDepotid()
    {
        return $this->Depotid;
    }

    /**
     * @param mixed $Depotid
     */
    public function setDepotid($Depotid)
    {
        $this->Depotid = $Depotid;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }


}
