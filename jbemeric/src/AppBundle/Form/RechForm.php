<?php

namespace AppBundle\Form;


use AppBundle\Entity\CylindreeRepository;
use AppBundle\Entity\MarqueRepository;
use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\PuissanceRepository;
use AppBundle\Entity\Type_moteurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Modele', EntityType::class, array(
                'class' => 'AppBundle:Modele',
                'query_builder' => function (ModeleRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',
                'placeholder'=>"---",
                'empty_data' => null,

            ))
            ->add('Marque', EntityType::class, array(
                'class' => 'AppBundle:Marque',
                'query_builder' => function (MarqueRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',
                'placeholder'=>"---",
                'empty_data' => 0,

            ))
            ->add('Cylindree', EntityType::class, array(
                'class' => 'AppBundle:Cylindree',
                'query_builder' => function (CylindreeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',
                'placeholder'=>"---",
                'empty_data' => 0,

            ))
            ->add('Puissance', EntityType::class, array(
                'class' => 'AppBundle:Puissance',
                'query_builder' => function (PuissanceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',
                'placeholder'=>"---",
                'empty_data' => 0,

            ))
            ->add('TypeMoteur', EntityType::class, array(
                'class' => 'AppBundle:Type_moteur',
                'query_builder' => function (Type_moteurRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',
                'placeholder'=>"---",
                'empty_data' => 0,

            ))

        ;
    }



    public function getName()
    {
        return 'recherche_form';
    }

}
