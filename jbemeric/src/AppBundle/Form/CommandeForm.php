<?php

namespace AppBundle\Form;


use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\ProduitRepository;
use AppBundle\Entity\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CommandeForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ref')
            ->add('dateLivraison', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('dateCommande', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('adresseLivraison')
            ->add('adresseCommande')
            ->add('Userid', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.enabled=1')
                        ->orderBy('u.username', 'ASC');
                },
                'choice_label' => 'username'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Commande'
        ));
    }

    public function getName()
    {
        return 'commande_form';
    }

}
