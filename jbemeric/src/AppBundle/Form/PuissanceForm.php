<?php

namespace AppBundle\Form;


use AppBundle\Entity\CylindreeRepository;
use AppBundle\Entity\ModeleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PuissanceForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('Cylindreeid', EntityType::class, array(
                'class' => 'AppBundle:Cylindree',
                'query_builder' => function (CylindreeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.Modeleid','m')
                        ->join('m.Marqueid','ma')
                        ->where('u.isDeleted=1')
                        ->orderBy('ma.designation', 'ASC');
                },
                'choice_label' => function ($model) {
                    return $model->getModeleid()->getMarqueid()->getDesignation(). ' -- ' .$model->getModeleid()->getDesignation(). ' -- ' . $model->getDesignation();
                }))
            ->add('file',FileType::class, array(
                'label' => 'Logo (png , jpg , JPEG ...)',
                'required'=>false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Puissance'
        ));
    }

    public function getName()
    {
        return 'puissance_form';
    }

}
