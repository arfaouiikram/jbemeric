<?php

namespace AppBundle\Form;


use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\PuissanceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeMoteurForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('Puissanceid', EntityType::class, array(
                'class' => 'AppBundle:Puissance',
                'query_builder' => function (PuissanceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.Cylindreeid','m')
                        ->join('m.Modeleid','mo')
                        ->join('mo.Marqueid','ma')
                        ->where('u.isDeleted=1')
                        ->orderBy('ma.designation', 'ASC');
                },
                'choice_label' => function ($model) {
                    return $model->getCylindreeid()->getModeleid()->getMarqueid()->getDesignation(). '   ||   ' .$model->getCylindreeid()->getModeleid()->getDesignation(). '   ||   ' .$model->getCylindreeid()->getDesignation(). '   ||   ' . $model->getDesignation();
                }))
            ->add('file',FileType::class, array(
                'label' => 'Logo (png , jpg , JPEG ...)',
                'required'=>false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Type_moteur'
        ));
    }

    public function getName()
    {
        return 'type_moteur_form';
    }

}
