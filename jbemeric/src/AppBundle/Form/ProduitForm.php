<?php

namespace AppBundle\Form;


use AppBundle\Entity\CylindreeRepository;
use AppBundle\Entity\MarqueRepository;
use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\PuissanceRepository;
use AppBundle\Entity\Type_moteurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('ref')
            ->add('ht')
            ->add('ttc')
            ->add('descriptif')
            ->add('fabricant')
            ->add('lien')
            ->add('norme')
            ->add('code')
            ->add('reforigine')
            ->add('reffabricant')
            ->add('Marqueid', EntityType::class, array(
                'class' => 'AppBundle:Marque',
                'query_builder' => function (MarqueRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',))
            ->add('Modeleid', EntityType::class, array(
                'class' => 'AppBundle:Modele',
                'query_builder' => function (ModeleRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.Marqueid','m')
                        ->where('u.isDeleted=1')
                        ->orderBy('m.designation', 'ASC');
                },
                'choice_label' => function ($model) {
                    return $model->getMarqueid()->getDesignation(). ' -- ' . $model->getDesignation();
                }))
            ->add('Cylindreeid', EntityType::class, array(
                'class' => 'AppBundle:Cylindree',
                'query_builder' => function (CylindreeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.Modeleid','m')
                        ->join('m.Marqueid','ma')
                        ->where('u.isDeleted=1')
                        ->orderBy('ma.designation', 'ASC');
                },
                'choice_label' => function ($model) {
                    return $model->getModeleid()->getMarqueid()->getDesignation(). ' -- ' .$model->getModeleid()->getDesignation(). ' -- ' . $model->getDesignation();
                }))
            ->add('Puissanceid', EntityType::class, array(
                'class' => 'AppBundle:Puissance',
                'query_builder' => function (PuissanceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.Cylindreeid','m')
                        ->join('m.Modeleid','mo')
                        ->join('mo.Marqueid','ma')
                        ->where('u.isDeleted=1')
                        ->orderBy('ma.designation', 'ASC');
                },
                'choice_label' => function ($model) {
                    return $model->getCylindreeid()->getModeleid()->getMarqueid()->getDesignation(). '   ||   ' .$model->getCylindreeid()->getModeleid()->getDesignation(). '   ||   ' .$model->getCylindreeid()->getDesignation(). '   ||   ' . $model->getDesignation();
                }))
            ->add('Type_moteurid', EntityType::class, array(
                'class' => 'AppBundle:Type_moteur',
                'query_builder' => function (Type_moteurRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.Puissanceid','p')
                        ->join('p.Cylindreeid','c')
                        ->join('c.Modeleid','mo')
                        ->join('mo.Marqueid','ma')
                        ->where('u.isDeleted=1')
                        ->orderBy('ma.designation', 'ASC');
                },
                'choice_label' => function ($model) {
                    return $model->getPuissanceid()->getCylindreeid()->getModeleid()->getMarqueid()->getDesignation().'   ||   ' .$model->getPuissanceid()->getCylindreeid()->getModeleid()->getDesignation(). '   ||   ' .$model->getPuissanceid()->getCylindreeid()->getDesignation(). '   ||   '.$model->getPuissanceid()->getDesignation().'  ||  ' .$model->getDesignation();
                }))            ->add('file',FileType::class, array(
                'label' => 'Logo (png , jpg , JPEG ...)',
                'required'=>false
            ))
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Produit'
        ));
    }

    public function getName()
    {
        return 'produit_form';
    }

}
