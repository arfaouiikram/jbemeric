<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Form\ClientForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\RechForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RequestContext;


class PaymentController extends Controller
{

    public function payementAction (Request $request ,$totale){
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() .$request->getBaseUrl();
        $user="arfaouiikram2016-acheteur_api1.gmail.com";
        $password="X5JZMLQQDKL5AM9G";
        $signature="AFcWxV21C7fd0v3bYYYRCpSSRl31AzgnTx-oZ1aJ9AyoLaI4Z56o24.A";
        /*  $user = $this->getUser();
          $em = $this->getDoctrine()->getManager();
          $dql = "SELECT l "
              . "FROM AppBundle:LigneCommande l"
              . " join l.Commandeid c"
              . " join c.Userid u"
              . " where u.id=".$user->getId()
              . " and c.isDeleted=1" ;
          $query = $em->createQuery($dql);
          $lignes = $query->getResult();*/
        $param=array(
            'METHOD'=>'SetExpressCheckout',
            'VERSION'=>'95.0',
            'USER'=>$user,
            'SIGNATURE'=>$signature,
            'PWD'=>$password,
            'RETURNURL'=>$baseurl.'/details?totale='.$totale,
            'CANCELURL'=>$baseurl.'/panier',
            'PAYMENTREQUEST_0_AMT'=>$totale,
            'PAYMENTREQUEST_0_CURRENCYCODE'=>'EUR',
            'PAYMENTREQUEST_0_ITEMAMT'=>$totale

        );
        /* foreach ($lignes as $k=>$ligne){
             $param["L_PAYMENTREQUEST_0_NAME$k"]=$ligne->getProduitid()->getDesignation();
             $param["L_PAYMENTREQUEST_0_AMT$k"]=$ligne->getProduitid()->getHt();
             $param["L_PAYMENTREQUEST_0_QTY$k"]=$ligne->getQuantite();
         }*/
        $param=http_build_query($param);
        $endponit='https://api-3T.sandbox.paypal.com/nvp';
        $curl=curl_init();
        curl_setopt_array($curl,array(
            CURLOPT_URL=>$endponit,
            CURLOPT_POST=>1,
            CURLOPT_POSTFIELDS=>$param,
            CURLOPT_RETURNTRANSFER=>1,
            CURLOPT_SSL_VERIFYPEER=>false,
            CURLOPT_SSL_VERIFYHOST=>false,
            CURLOPT_VERBOSE=>1
        ));
        $response=curl_exec($curl);
        $tab=array();
        parse_str($response,$tab);
        $paypal="https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&useraction=commit&token=".$tab['TOKEN'];
        return $this->redirect($paypal);
    }

    public function detailsAction (Request $request ){
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() .$request->getBaseUrl();
        $user="arfaouiikram2016-acheteur_api1.gmail.com";
        $password="X5JZMLQQDKL5AM9G";
        $signature="AFcWxV21C7fd0v3bYYYRCpSSRl31AzgnTx-oZ1aJ9AyoLaI4Z56o24.A";
        $em = $this->getDoctrine()->getManager();
        $username = $this->getUser();
        if($user){
            $param=array(
                'METHOD'=>'GetExpressCheckoutDetails',
                'VERSION'=>'95.0',
                'TOKEN'=>$_GET['token'],
                'USER'=>$user,
                'SIGNATURE'=>$signature,
                'PWD'=>$password,
                'RETURNURL'=>$baseurl.'/payed',
                'CANCELURL'=>$baseurl.'/panier',
                'PAYMENTREQUEST_0_AMT'=>$_GET['totale'],
                'PAYMENTREQUEST_0_CURRENCYCODE'=>'EUR'

            );
            $param=http_build_query($param);
            $endponit='https://api-3T.sandbox.paypal.com/nvp';
            $curl=curl_init();
            curl_setopt_array($curl,array(
                CURLOPT_URL=>$endponit,
                CURLOPT_POST=>1,
                CURLOPT_POSTFIELDS=>$param,
                CURLOPT_RETURNTRANSFER=>1,
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_SSL_VERIFYHOST=>false,
                CURLOPT_VERBOSE=>1
            ));
            $response=curl_exec($curl);
            $tab=array();
            parse_str($response,$tab);
            if($tab['CHECKOUTSTATUS']!='PaymentActionCompleted'){
                //payment
                $param=array(
                    'METHOD'=>'DoExpressCheckoutPayment',
                    'VERSION'=>'95.0',
                    'TOKEN'=>$_GET['token'],
                    'PAYERID'=>$_GET['PayerID'],
                    'PAYMENTACTION'=>'Sale',
                    'USER'=>$user,
                    'SIGNATURE'=>$signature,
                    'PWD'=>$password,
                    'RETURNURL'=>$baseurl.'/payed',
                    'CANCELURL'=>$baseurl.'/panier',
                    'PAYMENTREQUEST_0_AMT'=>$_GET['totale'],
                    'PAYMENTREQUEST_0_CURRENCYCODE'=>'EUR'
                );
                $param=http_build_query($param);
                $curl=curl_init();
                curl_setopt_array($curl,array(
                    CURLOPT_URL=>$endponit,
                    CURLOPT_POST=>1,
                    CURLOPT_POSTFIELDS=>$param,
                    CURLOPT_RETURNTRANSFER=>1,
                    CURLOPT_SSL_VERIFYPEER=>false,
                    CURLOPT_SSL_VERIFYHOST=>false,
                    CURLOPT_VERBOSE=>1
                ));
                $response=curl_exec($curl);
                $tab=array();
                parse_str($response,$tab);
                $dql = "SELECT l "
                    . "FROM AppBundle:LigneCommande l"
                    . " join l.Commandeid c"
                    . " join c.Userid u"
                    . " where u.id=".$username->getId()
                    . " and c.isDeleted=1" ;
                $query = $em->createQuery($dql);
                $lignes = $query->getResult();
                foreach ($lignes as $l){
                    $l->getCommandeid()->setIsDeleted(0);
                    $em->persist($l->getCommandeid());
                    $em->remove($l);
                }
                $em->flush();


            }

            return $this->redirect($this->generateUrl('kalitys_crm_homepage'));
        }
        return $this->redirect($this->generateUrl('simulation_crm_list_panier'));
    }


}

