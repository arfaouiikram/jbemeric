<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Depot;
use AppBundle\Form\ClientForm;
use AppBundle\Form\DepotForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class DepotController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Depot m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/depot.html.twig",array('pagination' => $results));
    }

    public function ajoutDepotAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $depot = new Depot();
        $form = $this->createForm(DepotForm::class, $depot);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $depot->setIsDeleted(1);
            $em->persist($depot);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Dépot ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $depot = new Depot();
                $form = $this->createForm(DepotForm::class, $depot);
                return $this->render('default/ajoutDepot.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_depot'));
        }
        return $this->render('default/ajoutDepot.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function deleteDepotAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $depot = $em->getRepository('AppBundle:Depot')->find($id);
        $depot->setIsDeleted(0);
        $em->persist($depot);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_depot"));
    }

    public function updateDepotAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $depot = $em->getRepository('AppBundle:Depot')->find($id);
        $form = $this->createForm(DepotForm::class, $depot);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $em->persist($depot);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Dépot modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_depot"));
        }
        return $this->render('default/ajoutDepot.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

}
