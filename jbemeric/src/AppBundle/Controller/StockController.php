<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Stock;
use AppBundle\Form\ClientForm;
use AppBundle\Form\StockForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class StockController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Stock m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/stock.html.twig",array('pagination' => $results));
    }

    public function ajoutStockAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $stock = new Stock();
        $form = $this->createForm(StockForm::class, $stock);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $stock->setIsDeleted(1);
            $em->persist($stock);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Stock ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $stock = new Stock();
                $form = $this->createForm(StockForm::class, $stock);
                return $this->render('default/ajoutStock.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_stock'));
        }
        return $this->render('default/ajoutStock.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function deleteStockAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $stock = $em->getRepository('AppBundle:Stock')->find($id);
        $stock->setIsDeleted(0);
        $em->persist($stock);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_stock"));
    }

    public function updateStockAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $stock = $em->getRepository('AppBundle:Stock')->find($id);
        $form = $this->createForm(StockForm::class, $stock);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $em->persist($stock);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Stock modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_stock"));

        }
        return $this->render('default/ajoutStock.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

}
