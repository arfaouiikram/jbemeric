<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Accessoire;
use AppBundle\Entity\Commande;
use AppBundle\Entity\Cylindree;
use AppBundle\Entity\LigneCommande;
use AppBundle\Entity\Logo;
use AppBundle\Entity\Modele;
use AppBundle\Entity\Produit;
use AppBundle\Entity\Puissance;
use AppBundle\Entity\Type_moteur;
use AppBundle\Form\AccessoireForm;
use AppBundle\Form\CommandeForm;
use AppBundle\Form\CylindreeForm;
use AppBundle\Form\LogoForm;
use AppBundle\Form\ModeleForm;
use AppBundle\Form\ProduitForm;
use AppBundle\Form\PuissanceForm;
use AppBundle\Form\RechForm;
use AppBundle\Form\TypeMoteurForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Marque;
use AppBundle\Form\MarqueForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute("fos_user_profile_show");
        /* $em = $this->get('doctrine.orm.entity_manager');
         $dql = "SELECT m "
             . "FROM AppBundle:Marque m where m.isDeleted=1" ;
         $query = $em->createQuery($dql);
         $form = $this->createForm(RechForm::class);
         $results=array();
         $user = $this->getUser();
         $display='block';
         $display1='none';
         if($user!=null) {
             $role = $user->getRoles();
             $display = 'none';
             $display1 = 'block';
             if ($role[0] == 'ROLE_SUPER_ADMIN') {
                 $display2 = 'block';
             return $this->render("default/home.html.twig", array('pagination' => $results, 'display' => $display, 'display1' => $display1, 'display2' => $display2, 'form' => $form->createView()));
             }
         }
         $display2='none';
         return $this->render("default/home.html.twig",array('pagination' => $results,'display'=>$display,'display1'=>$display1,'display2'=>$display2,'form'=>$form->createView()));*/
    }

    public function adminAction(Request $request)
    {
        $user = $this->getUser();
        if ($user != null) {
            $role = $user->getRoles();
            if ($role[0] == 'ROLE_SUPER_ADMIN') {
                return $this->render("default/dash.html.twig");
            }
        }
        return $this->redirectToRoute("kalitys_crm_frontpage");
    }

    public function frontAction()
    {
        $user = $this->getUser();
        if ($user != null) {
            $display = 'none';
            $display1 = 'block';
            $display2 = 'none';
        } else {
            $display = 'block';
            $display1 = 'none';
            $display2 = 'none';
        }
        $dql = "SELECT m "
            . "FROM AppBundle:Produit m where m.isDeleted=1 and m.topNov=1";
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->createQuery($dql);
        $results1 = $query->getResult();
        $results = array();
        $form = $this->createForm(RechForm::class);
        return $this->render("default/home.html.twig", array('pagination' => $results, 'pagination1' => $results1, 'display' => $display, 'display1' => $display1, 'display2' => $display2, 'form' => $form->createView()));

    }

    public function indexModeleAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Modele m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/modele.html.twig", array('pagination' => $results));
    }

    public function indexMarqueAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Marque m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/marque.html.twig", array('pagination' => $results));
    }

    public function indexCylindreeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Cylindree m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/cylindree.html.twig", array('pagination' => $results));
    }

    public function indexPuissanceAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Puissance m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/puissance.html.twig", array('pagination' => $results));
    }

    public function indexTypeMoteurAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Type_moteur  m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/typeMoteur.html.twig", array('pagination' => $results));
    }

    public function indexProduitAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Produit  m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/produit.html.twig", array('pagination' => $results));
    }

    public function indexLogoAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Logo  m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/logo.html.twig", array('pagination' => $results));
    }

    public function indexAccessoireAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Accessoire  m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/accessoire.html.twig", array('pagination' => $results));
    }

    public function indexCommandeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Commande  m where m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/commande.html.twig", array('pagination' => $results));
    }

    public function ajoutMarqueAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $marque = new Marque();
        $form = $this->createForm(MarqueForm::class, $marque);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $marque->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/marque',
                    $fileName
                );
                $marque->setLogo($fileName);
                $marque->setFile(null);
            }
            $marque->setIsDeleted(1);
            $em->persist($marque);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $marque = new Marque();
                $form = $this->createForm(MarqueForm::class, $marque);
                return $this->render('default/ajoutMarque.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_marque'));
        }
        return $this->render('default/ajoutMarque.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutModeleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $modele = new Modele();
        $form = $this->createForm(ModeleForm::class, $modele);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $modele->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/modele',
                    $fileName
                );
                $modele->setLogo($fileName);
                $modele->setFile(null);
            }
            $modele->setIsDeleted(1);
            $em->persist($modele);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $modele = new Modele();
                $form = $this->createForm(ModeleForm::class, $modele);
                return $this->render('default/ajoutModele.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_modele'));
        }
        return $this->render('default/ajoutModele.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutCylindreeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cylindree = new Cylindree();
        $form = $this->createForm(CylindreeForm::class, $cylindree);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $cylindree->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/cylindree',
                    $fileName
                );
                $cylindree->setLogo($fileName);
                $cylindree->setFile(null);
            }
            $cylindree->setIsDeleted(1);
            $em->persist($cylindree);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Cylindrée ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $cylindree = new Cylindree();
                $form = $this->createForm(CylindreeForm::class, $cylindree);
                return $this->render('default/ajoutCylindree.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_cylindree'));
        }
        return $this->render('default/ajoutCylindree.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutPuissanceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $puissance = new Puissance();
        $form = $this->createForm(PuissanceForm::class, $puissance);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $puissance->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/puissance',
                    $fileName
                );
                $puissance->setLogo($fileName);
                $puissance->setFile(null);
            }
            $puissance->setIsDeleted(1);
            $em->persist($puissance);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Puissance ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $puissance = new Puissance();
                $form = $this->createForm(PuissanceForm::class, $puissance);
                return $this->render('default/ajoutPuissance.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_puissance'));
        }

        return $this->render('default/ajoutPuissance.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutTypeMoteurAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $type = new Type_moteur();
        $form = $this->createForm(TypeMoteurForm::class, $type);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $type->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/typeMoteur',
                    $fileName
                );
                $type->setLogo($fileName);
                $type->setFile(null);
            }
            $type->setIsDeleted(1);
            $em->persist($type);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Type Moteur ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $type = new Type_moteur();
                $form = $this->createForm(TypeMoteurForm::class, $type);
                return $this->render('default/ajoutTypeMoteur.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_type_moteur'));
        }
        return $this->render('default/ajoutTypeMoteur.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutAccessoireAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $accessoire = new Accessoire();
        $form = $this->createForm(AccessoireForm::class, $accessoire);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $accessoire->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/accessoire',
                    $fileName
                );
                $accessoire->setLogo($fileName);
                $accessoire->setFile(null);
            }
            $accessoire->setIsDeleted(1);
            $em->persist($accessoire);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Accéssoire ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $type = new Accessoire();
                $form = $this->createForm(AccessoireForm::class, $type);
                return $this->render('default/ajoutAccessoire.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_accessoire'));
        }
        return $this->render('default/ajoutAccessoire.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutLogoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $logo = new Logo();
        $form = $this->createForm(LogoForm::class, $logo);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $logo->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/logo',
                    $fileName
                );
                $logo->setSrc($fileName);
                $logo->setFile(null);
            }
            $logo->setIsDeleted(1);
            $em->persist($logo);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Logo ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $logo = new Logo();
                $form = $this->createForm(LogoForm::class, $logo);
                return $this->render('default/ajoutLogo.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_logo'));
        }
        return $this->render('default/ajoutLogo.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block'));
    }

    public function ajoutProduitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $produit = new Produit();
        $form = $this->createForm(ProduitForm::class, $produit);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $produit->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/produit',
                    $fileName
                );
                $produit->setLogo($fileName);
                $produit->setFile(null);
            }
            $produit->setIsDeleted(1);
            $em->persist($produit);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "¨Produit ajouté avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $produit = new Produit();
                $form = $this->createForm(ProduitForm::class, $produit);
                return $this->render('default/ajoutProduit.html.twig', array('form' => $form->createView(), 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_produit'));
        }
        return $this->render('default/ajoutProduit.html.twig', array('form' => $form->createView(), 'display' => 'block', 'logo' => null));
    }

    public function ajoutCommandeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commande = new Commande();
        $form = $this->createForm(CommandeForm::class, $commande);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $commande->setIsDeleted(1);
            $em->persist($commande);
            $counter = $request->request->get('counter');
            for ($i = 0; $i < $counter; $i++) {
                $id = $request->request->get('browser' . $i);
                $produit = $em->getRepository('AppBundle:Produit')->find((int)$id);
                $quatite = $request->request->get('quatite' . $i);
                $prix = $request->request->get('prix' . $i);
                $prixTotal = $prix * $quatite;
                $ligne = new LigneCommande();
                $ligne->setProduitid($produit);
                $ligne->setPrix($prix);
                $ligne->setQuantite($quatite);
                $ligne->setTotal($prixTotal);
                $ligne->setCommandeid($commande);
                $em->persist($ligne);
            }
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "¨Commande ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $commande = new Commande();
                $form = $this->createForm(CommandeForm::class, $commande);
                return $this->render('default/ajoutCommande.html.twig', array('form' => $form->createView(), 'display' => 'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_commande'));
        }
        return $this->render('default/ajoutCommande.html.twig', array('form' => $form->createView(), 'display' => 'block', 'pagination' => null));
    }

    public function ajoutPanier1Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if ($user != null) {

            $dqlP = "SELECT l "
                . "FROM AppBundle:LigneCommande l"
                . " join l.Commandeid c"
                . " join c.Userid u"
                . " where u.id=" . $user->getId()
                . " and c.isDeleted=1";
            $queryp = $em->createQuery($dqlP);
            $ligneP = $queryp->getResult();
            $total = 0;
            foreach ($ligneP as $l) {
                $total += $l->getTotal();
            }
            return $this->render("default/panier.html.twig", array('pagination' => $ligneP, 'display' => "block", 'display1' => 'none', 'total' => $total));
        }
        return $this->redirectToRoute("fos_user_profile_show");

    }

    public function ajoutPanierAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cookies=$request->cookies;
        $produits=$em->getRepository('AppBundle:Produit')->findAll();
        $lignes=array();
        foreach($produits as $produit){
            $ligne=$cookies->get($produit->getId());
            if($ligne){
                $lignes[]=['produit'=>$produit,'quantite'=>$ligne,'tot'=>$produit->getTtc()*$ligne];
            }
        }
        $total=0;
        foreach($lignes as $l){
            $total=$total+$l['tot'];
        }
        return $this->render("default/panier.html.twig", array('pagination' => $lignes, 'display' => "block", 'display1' => 'none', 'total' => $total));
    }
    public function AccéePanierAfterAddAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if($user!=null)
       {
           /* $dql = "SELECT s "
                . "FROM AppBundle:Stock  s"
                . " join s.Produitid p"
                . " where p.id=".$id
                . " and s.isDeleted=1" ;
            $query = $em->createQuery($dql);*/
           // $stock = $query->getResult();
           // if(sizeof($stock)!=0){
               // if($stock[0]->getQuantite()>=$quantite){
                   $cookies=$request->cookies;
                   $produits=$em->getRepository('AppBundle:Produit')->findAll();
                   $lignes=array();
                   foreach($produits as $prod){
                       $lig=$cookies->get($prod->getId());
                       if($lig){
                           $lignes[]=['produit'=>$prod,'quantite'=>$lig,'tot'=>$prod->getTtc()*$lig];
                           $commande = new Commande();
                           $commande->setIsDeleted(1);
                           $commande->setUserid($user);
                           $em->persist($commande);
                           $ligne=new LigneCommande();
                           $ligne->setProduitid($prod);
                           $ligne->setPrix($prod->getTtc());
                           $ligne->setQuantite($lig);
                           $ligne->setTotal($lig*$prod->getTtc());
                           $ligne->setCommandeid($commande);
                           $em->persist($ligne);
                           $em->flush();
                       }
                   }
                   $total=0;
                   foreach($lignes as $l){
                       $total=$total+$l['tot'];
                   }
                   return $this->redirectToRoute("simulation_crm_paypal",array('totale'=>$total));
      // }
         //   }
        }
        return $this->redirectToRoute("fos_user_profile_show");
    }
    public function AddPanierAction(Request $request,$id,$quantite) {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        $tab=array();
        $tab[]=['produit'=>$produit->getId(),'quantite'=>$quantite];
        $cookie_info = array(
            'name' => $produit->getId(), // Nom du cookie
            'value' => $quantite, // Valeur du cookie
            'time' => time() + 3600 * 24 * 7 // Durée de vie du cookie
        );

        $cookie = new Cookie($cookie_info['name'], $cookie_info['value'], $cookie_info['time']);

        $response = new Response();
        $response->headers->setCookie($cookie);
        $response->sendHeaders();
        $result[]=['quantite'=>$quantite,'total'=>$produit->getTtc()*$quantite];
        return new JsonResponse($result);
    }



        public function listPanierAction(){

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
       // if($user!=null) {
            $dql = "SELECT l "
                . "FROM AppBundle:LigneCommande l"
                . " join l.Commandeid c"
                . " join c.Userid u"
                . " where u.id=".$user->getId()
                . " and c.isDeleted=1" ;
            $query = $em->createQuery($dql);
            $ligne = $query->getResult();
            $total=0;
            foreach ($ligne as $l){
                $total+=$l->getTotal();
            }
            return $this->render("default/panier.html.twig",array('pagination' => $ligne,'display'=>"block",'display1'=>'none','total'=>$total));
       // }
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Marque m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $form = $this->createForm(RechForm::class);
        $results=array();
        return $this->render("default/home.html.twig",array('pagination' => $results,'display'=>"none",'display1'=>'block','form'=>$form->createView()));
    }


    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function deleteMarqueAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $marque = $em->getRepository('AppBundle:Marque')->find($id);
        $marque->setIsDeleted(0);
        $em->persist($marque);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_marque"));
    }

    public function deleteModeleAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('AppBundle:Modele')->find($id);
        $modele->setIsDeleted(0);
        $em->persist($modele);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_modele"));
    }

    public function deleteCylindreeAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $cylindree = $em->getRepository('AppBundle:Cylindree')->find($id);
        $cylindree->setIsDeleted(0);
        $em->persist($cylindree);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_cylindree"));
    }

    public function deletePuissanceAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $puissance = $em->getRepository('AppBundle:Puissance')->find($id);
        $puissance->setIsDeleted(0);
        $em->persist($puissance);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_puissance"));
    }

    public function deleteTypeMoteurAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Type_moteur')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_type_moteur"));
    }

    public function deleteProduitAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Produit')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_produit"));
    }

    public function deleteAccessoireAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Accessoire')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_accessoire"));
    }

    public function deleteLogoAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Logo')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_logo"));
    }

    public function deleteCommandeAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Commande')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_commande"));

    }

    public function deletePanierAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Commande')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("simulation_crm_list_panier"));

    }

    public function updateMarqueAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $marque = $em->getRepository('AppBundle:Marque')->find($id);
        $form = $this->createForm(MarqueForm::class, $marque);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $marque->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/marque',
                    $fileName
                );
                $marque->setLogo($fileName);
                $marque->setFile(null);
            }
            $em->persist($marque);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_marque"));

        }
        return $this->render('default/ajoutMarque.html.twig', array('form' => $form->createView(),'logo'=>$marque->getLogo(),'display'=>'none'));
    }

    public function updateModeleAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository('AppBundle:Modele')->find($id);
        $form = $this->createForm(ModeleForm::class, $modele);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $modele->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/modele',
                    $fileName
                );
                $modele->setLogo($fileName);
                $modele->setFile(null);
            }
            $em->persist($modele);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Modèle modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_modele"));

        }
        return $this->render('default/ajoutModele.html.twig', array('form' => $form->createView(),'logo'=>$modele->getLogo(),'display'=>'none'));
    }

    public function updateCylindreeAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $cylindree = $em->getRepository('AppBundle:Cylindree')->find($id);
        $form = $this->createForm(CylindreeForm::class, $cylindree);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $cylindree->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/cylindree',
                    $fileName
                );
                $cylindree->setLogo($fileName);
                $cylindree->setFile(null);
            }
            $em->persist($cylindree);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Cylindée modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_cylindree"));

        }
        return $this->render('default/ajoutCylindree.html.twig', array('form' => $form->createView(),'logo'=>$cylindree->getLogo(),'display'=>'none'));
    }

    public function updatePuissanceAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $puissance = $em->getRepository('AppBundle:Puissance')->find($id);
        $form = $this->createForm(PuissanceForm::class, $puissance);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $puissance->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/puissance',
                    $fileName
                );
                $puissance->setLogo($fileName);
                $puissance->setFile(null);
            }
            $em->persist($puissance);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Puissance modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_puissance"));

        }
        return $this->render('default/ajoutPuissance.html.twig', array('form' => $form->createView(),'logo'=>$puissance->getLogo(),'display'=>'none'));
    }

    public function updateTypeMoteurAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Type_moteur')->find($id);
        $form = $this->createForm(TypeMoteurForm::class, $type);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $type->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/typeMoteur',
                    $fileName
                );
                $type->setLogo($fileName);
                $type->setFile(null);
            }
            $em->persist($type);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Type moteur modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_type_moteur"));

        }
        return $this->render('default/ajoutTypeMoteur.html.twig', array('form' => $form->createView(),'logo'=>$type->getLogo(),'display'=>'none'));
    }

    public function updateAccessoireAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $accessoire = $em->getRepository('AppBundle:Accessoire')->find($id);
        $form = $this->createForm(AccessoireForm::class, $accessoire);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $accessoire->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/accessoire',
                    $fileName
                );
                $accessoire->setLogo($fileName);
                $accessoire->setFile(null);
            }
            $em->persist($accessoire);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Accéssoire modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_accessoire"));

        }
        return $this->render('default/ajoutAccessoire.html.twig', array('form' => $form->createView(),'logo'=>$accessoire->getLogo(),'display'=>'none'));
    }

    public function updateLogoAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $logo = $em->getRepository('AppBundle:Logo')->find($id);
        $form = $this->createForm(LogoForm::class, $logo);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $logo->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/logo',
                    $fileName
                );
                $logo->setSrc($fileName);
                $logo->setFile(null);
            }
            $em->persist($logo);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Logo modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_logo"));

        }
        return $this->render('default/ajoutLogo.html.twig', array('form' => $form->createView(),'logo'=>$logo->getSrc(),'display'=>'none'));
    }
    public function updateProduitAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        $form = $this->createForm(ProduitForm::class, $produit);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $produit->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/produit',
                    $fileName
                );
                $produit->setLogo($fileName);
                $produit->setFile(null);
            }
            $em->persist($produit);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Produit modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_produit"));

        }
        return $this->render('default/ajoutProduit.html.twig', array('form' => $form->createView(),'display'=>'none','logo'=>$produit->getLogo()));
    }

    public function updateCommandeAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('AppBundle:Commande')->find($id);
        $dql = "SELECT m "
            . "FROM AppBundle:LigneCommande m "
            ."join m.Commandeid c "
            ."where c.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $form = $this->createForm(CommandeForm::class, $commande);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $em->persist($commande);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Commande modofiéee avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_commande"));

        }
        return $this->render('default/ajoutCommande.html.twig', array('form' => $form->createView(),'display'=>'none','pagination'=>$results));
    }

    public function  showAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        $dql = "SELECT i "
            . "FROM AppBundle:Logo i "
            ."join i.Produitid p "
            ."where p.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render('default/showCommande.html.twig', array('produit'=>$produit,'display'=>'block','display1'=>'none','pagination'=>$results));

    }

    public function fillProduitAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Produit  m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $data=array();
        $i=0;
        foreach ($results as $prod ) {
            $data[$i++]=["key"=>$prod->getId(),"value"=>$prod->getDesignation(),'prix'=>$prod->getHt()];
        }
        return new JsonResponse($data);

    }

    public function getPriceAction(Request $request,$id) {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Produit  m where m.isDeleted=1 and m.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return new JsonResponse($results[0]->getHt());

    }

    public function topVenteAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Produit')->find($id);
        $type->setTopNov(1);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_produit"));
    }

    public function apercuAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Produit')->find($id);
        $stock = $em->getRepository('AppBundle:Stock')->findBy(array('Produitid'=>$type));
        $logos = $em->getRepository('AppBundle:Logo')->findBy(array('Produitid'=>$type));
        return $this->render('default/popUpProduit.html.twig',array('produit'=>$type,'quantite'=>$stock?$stock[0]->getQuantite():0,'logos'=>$logos));
    }

    public function ficheAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Produit')->find($id);
        $stock = $em->getRepository('AppBundle:Stock')->findBy(array('Produitid'=>$type));
        $logos = $em->getRepository('AppBundle:Logo')->findBy(array('Produitid'=>$type));
        return $this->render('default/fiche.html.twig',array('produit'=>$type,'quantite'=>$stock?$stock[0]->getQuantite():0,'logos'=>$logos,'display'=>'block','display1'=>'none'));
    }


}
