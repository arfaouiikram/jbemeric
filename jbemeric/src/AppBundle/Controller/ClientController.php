<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Form\ClientForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ClientController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Client m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/client.html.twig",array('pagination' => $results));
    }

    public function ajoutClientAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $client = new Client();
        $form = $this->createForm(ClientForm::class, $client);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $client->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/client',
                    $fileName
                );
                $client->setLogo($fileName);
                $client->setFile(null);
            }
            $client->setIsDeleted(1);
            $em->persist($client);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $client = new Client();
                $form = $this->createForm(ClientForm::class, $client);
                return $this->render('default/ajoutClient.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_client'));
        }
        return $this->render('default/ajoutClient.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function deleteClientAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:Client')->find($id);
        $client->setIsDeleted(0);
        $em->persist($client);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_client"));
    }

    public function updateClientAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:Client')->find($id);
        $form = $this->createForm(ClientForm::class, $client);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $client->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/client',
                    $fileName
                );
                $client->setLogo($fileName);
                $client->setFile(null);
            }
            $em->persist($client);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_client"));

        }
        return $this->render('default/ajoutClient.html.twig', array('form' => $form->createView(),'logo'=>$client->getLogo(),'display'=>'none'));
    }

}
